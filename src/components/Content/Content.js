import React, {useEffect, useState, useCallback} from 'react';
import './Content.css';
import axios from "axios";

const Content = ({countryId}) => {

    const [countryData, setCountryData] = useState(null);

    const getCountry = useCallback(
        async () => {
            if (countryId !== null) {
                const url = 'https://restcountries.eu/rest/v2/alpha/';
                const responseCountryData = await axios.get(url + countryId);
                setCountryData(responseCountryData.data);
            }
        }, [countryId]);

    useEffect(() => {
        getCountry().catch(console.error);
    }, [countryId, getCountry]);

    return countryData && (
        <div className="Content">
            <h3>{countryData.name}</h3>
            <p>Capital: {countryData.capital}</p>
            <p>Population: {countryData.population}</p>
            <img src={countryData.flag} alt={countryData.name} width={350} height={200}/>
            <p>Borders with:</p>
            <ul>
                {countryData["borders"].length > 0 ? countryData["borders"].map(border => <li>{border}</li>) : <li>No borders</li>}
            </ul>
        </div>
    );
};

export default Content;