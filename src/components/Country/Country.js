import React from 'react';
import './Country.css';

const Country = props => {
    return (
        <li className="Country" onClick={props.countryInfo}>
            {props.children}
        </li>
    );
};

export default Country;