import React, {useEffect, useState} from 'react';
import axios from "axios";

import Content from "../../components/Content/Content";
import Country from "../../components/Country/Country";

import './App.css';

const App = () => {

    const [countries, setCountries] = useState([]);

    const [content, setContent] = useState(null);

    useEffect(() => {
        const requestCountries = async () => {
            const response = await axios.get('https://restcountries.eu/rest/v2/all?fields=name%3Balpha3Code');
            setCountries(response.data);
        }
        requestCountries().catch(console.error);
    }, []);


    let countryList = <li>Loading data</li>;

    if (countries) {
        countryList = countries.map(country => (
            <Country
                key={country['alpha3Code']}
                countryInfo={() => setContent(country['alpha3Code'])}
            >
                {country.name}
            </Country>
        ));
    }

    return (
        <div className="App">
            <div className="sidebar">
                <ul>
                    {countryList}
                </ul>
            </div>
            <div className="content">
                <Content countryId={content} />
            </div>
        </div>
    );
};

export default App;